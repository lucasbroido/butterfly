class Particle {
  constructor(x, y, ex, ey, h, s, l) {
    this.pos = createVector(x, y);
    this.end = createVector(ex, ey);
    this.velocity = createVector(0, 0);
    this.acc = createVector(0, 0);
    this.maxSpeed = 15;
    this.maxDistance = 10;
    this.maxForce = 15;
    this.hue = h;
    this.sat = s;
    this.light = l;
  }

  seek(target) {
    let force = new p5.Vector.sub(target, this.pos);
    let d = force.mag();
    let speed = this.maxSpeed;
    if (d == 0) return;
    else if (d < this.maxDistance) {
      speed = map(d, 0, 10, 0, this.maxSpeed);
      force.setMag(speed);
    } 
    else {
      force.setMag(this.maxSpeed);
    }
    force.sub(this.velocity);

    force.limit(this.maxForce);
    this.acc.add(force);
  }

  flee(target, force) {
    force.setMag(this.maxSpeed);
    force.sub(this.velocity);
    force.limit(this.maxForce).mult(random(-4, -1));

    this.acc.add(force);
  }

  update(mousePosition) {
    this.pos.add(this.velocity.add(this.acc));
    this.acc.set(0, 0);
    this.seek(this.end);
  }

  show() {
    stroke(this.hue, this.sat, this.light);
    point(this.pos.x, this.pos.y);
  }
}
