let particles = [];
let flee_slider, blur_slider;
let mousePosition;
const pdcty = 6.28;
const increment = 0.008;

function setup() {
  createCanvas(window.innerWidth, window.innerHeight);
  fill(0, 360, 200);
  colorMode(HSL, 360);
  fill(0, 360, 200);

  flee_slider = createSlider(10, 100, 50);
  flee_slider.position(width - width / 5, height - height / 10);

  blur_slider = createSlider(0, 255, 255);
  blur_slider.position(width - width / 5, height - height / 6);

  let hue = 0;
  let m = 15;
  let n = 22;
  let deviation = 400;

  for (let i = m * pdcty; i < n * pdcty; i += increment) {


    // butterfly
    let px = cos(i) * (exp(cos(i)) - 2 * cos(4 * i) - pow(sin(i / 12), 5.0));
    let py = sin(i) * (exp(cos(i)) - 2 * cos(4 * i) - pow(sin(i / 12), 5.0));

    let v = createVector(px, py);
    v.mult(width / 15);
  
    // generate particles within a deviation radius of their final resting position
    let rx = v.x+random(-1,1)*deviation;
    let ry = v.y+random(-1,1)*deviation;

    if (hue >= 360) {
      hue = 0;
    } else hue = hue + 10;

    let particle = new Particle(rx, ry, v.x, v.y, hue, 360, 200);
    particles.push(particle);
  }
}

function draw() {
  translate(width / 2, height / 2);
  rotate(-PI / 2);
  background(0, blur_slider.value());

  text("blur slider", blur_slider.x, blur_slider.y - 50);


  for (let i = 0; i < particles.length; i++) {
    let distanceFromOrigin = new p5.Vector.sub(
      particles[i].end,
      particles[i].pos
    ).mag();


    if (distanceFromOrigin > flee_slider.value()) {
      let sw = map(distanceFromOrigin, 0, width / 4, 3, 30);
      strokeWeight(sw);
    } else strokeWeight(3);

    particles[i].update(mousePosition, distanceFromOrigin);
    particles[i].show();
  }

}

function mouseMoved(event) {
    mousePosition = createVector(height / 2 - mouseY, mouseX - width / 2);

    particles.forEach(p => {
      
      let forceFromMouse = new p5.Vector.sub(mousePosition, p.pos);

      if (forceFromMouse.mag() < flee_slider.value()) {
        p.flee(mousePosition, forceFromMouse);
      }
    })
    
    }
